const logger = {
    info: (...x) =>  console.log(...x),
    debug: (...x) =>  console.log(...x),
    error: (...x) =>  console.log(...x),
    fatal: (...x) =>  console.log(...x),
  };
  
  export default logger;
  